/* Do not touch this file */

package com.gildedrose;

public class Item {

    public static final String SULFURAS_HAND_OF_RAGNAROS = "Sulfuras, Hand of Ragnaros";
    public static final String BACKSTAGE = "Backstage passes to a TAFKAL80ETC concert";
    public static final String AGED_BRIE = "Aged Brie";
    public static final String CONJURED = "Conjured";
    public String name;

    public int sellIn;

    public int quality;

    public Item(String name, int sellIn, int quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    /**
     * Increase quality attribute
     */
    private void increaseQuality() {
        if (quality < 50 && !name.equals(SULFURAS_HAND_OF_RAGNAROS)) {
            quality++;
        }
    }

    /**
     * Decrease quality attribute
     */
    private void decreaseQuality() {
        decreaseQuality(1);
    }

    /**
     * Decrease quality by value
     *
     * @param decrease Value to decrease
     */
    private void decreaseQuality(int decrease) {
        if (quality > decrease - 1 && !name.equals(SULFURAS_HAND_OF_RAGNAROS)) {
            quality = quality - decrease;
        }
    }

    /**
     * Update quality of any item
     */
    public void updateQuality() {
        decreaseSellIn();
        switch (name) {
            case AGED_BRIE -> updateQualityAgedBrie();
            case BACKSTAGE -> updateQualityBackstage();
            case CONJURED -> {
                decreaseQuality(2);
                if (sellIn < 0) decreaseQuality(2);
            }
            default -> {
                decreaseQuality();
                if (sellIn < 0) decreaseQuality();
            }
        }
    }

    /**
     * Change quality of aged brie
     */
    private void updateQualityAgedBrie() {
        increaseQuality();
        if (sellIn < 0) {
            increaseQuality();
        }
    }

    /**
     * Change quality of backstage
     */
    private void updateQualityBackstage() {
        if (sellIn < 0) {
            quality = 0;
        } else {
            increaseQuality();
            if (sellIn < 10) {
                increaseQuality();
            }
            if (sellIn < 5) {
                increaseQuality();
            }
        }
    }

    /**
     * Decrease sellin
     */
    private void decreaseSellIn() {
        if (!name.equals(SULFURAS_HAND_OF_RAGNAROS)) {
            sellIn--;
        }
    }

    @Override
    public String toString() {
        return this.name + ", " + this.sellIn + ", " + this.quality;
    }
}
