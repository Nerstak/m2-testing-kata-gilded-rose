package com.gildedrose;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

class ApplicationTest {
    static PrintStream old;
    static ByteArrayOutputStream baos;

    @BeforeAll
    static void init() {
        baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);

        old = System.out;
        System.setOut(ps);
    }

    @AfterAll
    static void destroy() {
        System.out.flush();
        System.setOut(old);
    }

    @Test
    void goldenMasterTest() throws IOException {
        Application.main(new String[]{});

        String currentOutput = baos.toString();
        Path path = Paths.get("expected-output.txt");
        String file = Files.readAllLines(path).stream().map(line -> line + System.lineSeparator()).collect(Collectors.joining());

        assertEquals(file, currentOutput, "Golden Master");
    }

}
