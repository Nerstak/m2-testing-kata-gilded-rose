package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    @Test
    void testSimpleItem() {
        Item foo = new Item("foo", 20, 10);

        foo.updateQuality();

        assertEquals(19,foo.sellIn);
        assertEquals(9, foo.quality);
    }

    @Test
    void testSellInAgedBrie() {
        Item foo = new Item("Aged Brie", 0, 10);

        foo.updateQuality();

        assertEquals(-1,foo.sellIn);
        assertEquals(12, foo.quality);
    }

    @Test
    void testSellInAgedBrieMaxQuality() {
        Item foo = new Item("Aged Brie", 0, 50);

        foo.updateQuality();

        assertEquals(-1,foo.sellIn);
        assertEquals(50, foo.quality);
    }

    @Test
    void testSellInBackstage() {
        Item foo = new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10);

        foo.updateQuality();

        assertEquals(-1,foo.sellIn);
        assertEquals(0, foo.quality);
    }

    @Test
    void testSellInSulfura() {
        Item foo = new Item("Sulfuras, Hand of Ragnaros", 0, 80);

        foo.updateQuality();

        assertEquals(0,foo.sellIn);
        assertEquals(80, foo.quality);
    }

    @Test
    void testSellInRegularItems() {
        Item foo = new Item("foo", 0, 13);

        foo.updateQuality();

        assertEquals(-1,foo.sellIn);
        assertEquals(11, foo.quality);
    }

    @Test
    void testQualityIncreaseBackstage10Days() {
        Item foo = new Item("Backstage passes to a TAFKAL80ETC concert", 10, 13);

        foo.updateQuality();

        assertEquals(9,foo.sellIn);
        assertEquals(15, foo.quality);
    }

    @Test
    void testQualityIncreaseBackstage5Days() {
        Item foo = new Item("Backstage passes to a TAFKAL80ETC concert", 5, 13);

        foo.updateQuality();

        assertEquals(4,foo.sellIn);
        assertEquals(16, foo.quality);
    }

    @Test
    void testQualityConjured() {
        Item foo = new Item("Conjured", 5, 13);

        foo.updateQuality();

        assertEquals(4,foo.sellIn);
        assertEquals(11, foo.quality);
    }

    @Test
    void testSellInConjured() {
        Item foo = new Item("Conjured", -5, 13);

        foo.updateQuality();

        assertEquals(-6,foo.sellIn);
        assertEquals(9, foo.quality);
    }
}
